package general;

import tree.statement.StatementNode;
import types.IntegerType;
import types.Type;
import values.IntegerValue;
import values.Value;

import java.util.Hashtable;

/**
 * Created by mac on 11/19/14.
 */
public class SymbolsTable
{

    private static SymbolsTable instance;

    private SymbolsTable()
    {

    }

    public static SymbolsTable getInstance()
    {
        if(instance==null)
            instance = new SymbolsTable();
        return instance;
    }

    Hashtable<String, Type> variablesMetadata= new Hashtable<String, Type>();

    Hashtable<String, Value> variablesValue= new Hashtable<String, Value>();

    Hashtable<String, Function> functionsMetadata= new Hashtable<String, Function>();


    public void declareVariable(String name,Type type) throws Exception {
       if(variablesMetadata.containsKey(name))
           throw new Exception("Id "+name+" ya esta declarado");

        variablesMetadata.put(name,type);
        variablesValue.put(name,type.getDefaultValue());

    }

    public Type getVariableType(String name) throws Exception {
        if(!variablesMetadata.containsKey(name))
            throw new Exception("Id "+name+" no esta declarado");
        return variablesMetadata.get(name);
    }

    public void declareFunction(String name,Function functionInfo) throws Exception {
        if(functionsMetadata.containsKey(name))
            throw new Exception("funcion "+name+" ya esta declarado");

        functionsMetadata.put(name,functionInfo);
    }

    public void ValidateSemanticOnFunctions() throws Exception
    {
        for( String name: functionsMetadata.keySet())
        {
              Function currentFuncion = functionsMetadata.get(name);
              //llenar la tabla local
              for(StatementNode stmnt: currentFuncion.getCode())
              {
                  stmnt.validateSemantics();
              }
              //vaciar tabla local
        }
    }


    public void setVariableValue(String name,Value value) throws Exception {
        variablesValue.put(name,value);
    }

    public Value getVariableValue(String name) throws Exception {

        return variablesValue.get(name);
    }


}
