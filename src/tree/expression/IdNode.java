/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tree.expression;

import enexceso.MemoryVars;
import general.SymbolsTable;
import types.Type;
import values.ArrayValue;
import values.IntegerValue;
import values.Value;

import java.util.List;



/**
 *
 * @author Eduardo
 */
public class IdNode extends ExpressionNode{
    //a[4][5][7]
    List<ExpressionNode> indices;

    public IdNode(List<ExpressionNode> indices, String name) {
        this.indices = indices;
        this.name = name;
    }

    public List<ExpressionNode> getIndices() {
        return indices;
    }

    public void setIndices(List<ExpressionNode> indices) {
        this.indices = indices;
    }
    
    //:(
    private String name;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
        
        
    }

    @Override
    public float evaluate() {
        return MemoryVars.getValue(name);
    }

    @Override
    public Type evaluateType() throws Exception {
        Type idType = SymbolsTable.getInstance().getVariableType(name);
        return idType;
    }

    @Override
    public Value interpretResult() {
        try {
           if(indices==null||indices.size()==0) {
               return SymbolsTable.getInstance().getVariableValue(name);
           }else
           {
               ArrayValue arrayValue = (ArrayValue) SymbolsTable.getInstance().getVariableValue(name);
               int i;
               for( i=0;i<indices.size()-1;i++)
               {
                   IntegerValue indexValue= (IntegerValue)indices.get(i).interpretResult();
                   arrayValue = (ArrayValue)arrayValue.getIndexValue(indexValue.getValue());
               }

               IntegerValue indexValue= (IntegerValue)indices.get(i).interpretResult();
               return arrayValue.getIndexValue(indexValue.getValue());

           }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

    public void setValue(float evaluate) {
        MemoryVars.setValue(name, evaluate);
    }


    public void assignValue(Value value) throws Exception    {
       if(indices==null || indices.size()==0) {
           //arr =1;
           SymbolsTable.getInstance().setVariableValue(name, value);
       }else {
           //arr[3][1][4] =1;
           ArrayValue arrayValue = (ArrayValue) SymbolsTable.getInstance().getVariableValue(name);
           int i;
           for( i=0;i<indices.size()-1;i++)
           {
               IntegerValue indexValue= (IntegerValue)indices.get(i).interpretResult();
               arrayValue = (ArrayValue)arrayValue.getIndexValue(indexValue.getValue());
           }

           IntegerValue indexValue= (IntegerValue)indices.get(i).interpretResult();
           arrayValue.setIndexValue(indexValue.getValue(),value);

       }
    }
}
