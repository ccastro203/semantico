/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tree.expression;

import types.FloatType;
import types.IntegerType;
import types.Type;
import values.IntegerValue;
import values.Value;

/**
 *
 * @author Eduardo
 */
public class MultNode extends BinaryOperatorNode{

    public MultNode(ExpressionNode raito, ExpressionNode leftou) {
        super(raito, leftou);
    }

    @Override
    public float evaluate() {
        return leftou.evaluate()*raito.evaluate();
    }

    @Override
    public Type evaluateType() throws Exception {
        Type rightType = raito.evaluateType();
        Type leftType =leftou.evaluateType();

        //int * int -> int
        if(rightType instanceof IntegerType &&
                leftType instanceof IntegerType)
        {
            return new IntegerType();
        }
        //float * float -> float
        if(rightType instanceof FloatType &&
                leftType instanceof FloatType)
        {
            return new FloatType();
        }

        throw new Exception("No se puede multplicar "
                +rightType+" con "+leftType);
    }

    @Override
    public Value interpretResult() {
        Value rightValue = raito.interpretResult();
        Value leftValue =leftou.interpretResult();

        if(rightValue instanceof IntegerValue && leftValue instanceof IntegerValue)
        {
            return new IntegerValue(
                    ((IntegerValue)leftValue).getValue()*
                            ((IntegerValue)rightValue).getValue()
            );
        }
        return null;
    }

}
