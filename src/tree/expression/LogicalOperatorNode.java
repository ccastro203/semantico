package tree.expression;

import types.*;
import values.Value;

/**
 * Created by mac on 11/20/14.
 */
public abstract class LogicalOperatorNode extends BinaryOperatorNode {
    public LogicalOperatorNode(ExpressionNode raito, ExpressionNode leftou) {
        super(raito, leftou);
    }



    @Override
    public Type evaluateType() throws Exception {
        Type rightType = raito.evaluateType();
        Type leftType =leftou.evaluateType();

        //int == int -> bool
        if(rightType instanceof IntegerType &&
                leftType instanceof IntegerType)
        {
            return new BooleanType();
        }
        //float == float -> bool
        if(rightType instanceof FloatType &&
                leftType instanceof FloatType)
        {
            return new BooleanType();
        }
        //str == str -> bool
        if(rightType instanceof StringType &&
                leftType instanceof StringType)
        {
            return new BooleanType();
        }
        //char == char -> bool
        if(rightType instanceof CharType &&
                leftType instanceof CharType)
        {
            return new BooleanType();
        }
        //bool == bool -> bool
        if(rightType instanceof BooleanType &&
                leftType instanceof BooleanType)
        {
            return new BooleanType();
        }

        throw new Exception("No se puede comparar "
                +rightType+" con "+leftType);
    }

    @Override
    public Value interpretResult() {
        return null;
    }
}
