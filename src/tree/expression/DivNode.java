/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tree.expression;

import types.FloatType;
import types.IntegerType;
import types.Type;
import values.Value;

/**
 *
 * @author Eduardo
 */
public class DivNode extends BinaryOperatorNode{

    public DivNode(ExpressionNode raito, ExpressionNode leftou) {
        super(raito, leftou);
    }

    @Override
    public float evaluate() {
        return leftou.evaluate()/raito.evaluate();
    }

    @Override
    public Type evaluateType() throws Exception {
        Type rightType = raito.evaluateType();
        Type leftType =leftou.evaluateType();

        //int / int -> int
        if(rightType instanceof IntegerType &&
                leftType instanceof IntegerType)
        {
            return new IntegerType();
        }
        //float / float -> float
        if(rightType instanceof FloatType &&
                leftType instanceof FloatType)
        {
            return new FloatType();
        }

        throw new Exception("No se puede dividir "
                +rightType+" con "+leftType);
    }

    @Override
    public Value interpretResult() {
        return null;
    }

}
