/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tree.statement;

import java.util.List;
import tree.expression.ExpressionNode;
import tree.expression.IdNode;
import types.IntegerType;
import types.Type;
import values.IntegerValue;
import values.Value;

/**
 *
 * @author Eduardo
 */
public class ForNode extends StatementNode{
    
    IdNode id;

    public ForNode(IdNode id, ExpressionNode initialValue, ExpressionNode finalValue, List<StatementNode> statements) {
        this.id = id;
        this.initialExpression = initialValue;
        this.finalExpression = finalValue;
        this.statements = statements;
    }

    public IdNode getId() {
        return id;
    }

    public void setId(IdNode id) {
        this.id = id;
    }

    public ExpressionNode getInitialExpression() {
        return initialExpression;
    }

    public void setInitialExpression(ExpressionNode initialExpression) {
        this.initialExpression = initialExpression;
    }

    public ExpressionNode getFinalExpression() {
        return finalExpression;
    }

    public void setFinalExpression(ExpressionNode finalExpression) {
        this.finalExpression = finalExpression;
    }

    public List<StatementNode> getStatements() {
        return statements;
    }

    public void setStatements(List<StatementNode> statements) {
        this.statements = statements;
    }
    ExpressionNode initialExpression;
    ExpressionNode finalExpression;
    List<StatementNode> statements;
    @Override
    public void evaluate() {
        id.setValue(initialExpression.evaluate());
        float value = id.evaluate();
        while (value <= finalExpression.evaluate()) {
            
            for(StatementNode node: statements)
            {
                node.evaluate();
            }
            
            value = id.evaluate();
            value++;
            id.setValue(value);
            
        }
    }

    @Override
    public void validateSemantics() throws Exception{
        Type idType = id.evaluateType();
        Type initialType = initialExpression.evaluateType();
        Type finalType = finalExpression.evaluateType();

        if(!(idType instanceof IntegerType))
               throw  new Exception("id tiene q ser entero");
        if(!(initialType instanceof IntegerType))
            throw  new Exception("initial tiene q ser entero");
        if(!(finalType instanceof IntegerType))
            throw  new Exception("final tiene q ser entero");
        for(StatementNode node: statements)
        {
            node.validateSemantics();
        }

    }

    @Override
    public void Interpret() throws Exception {
        IntegerValue initialValue =(IntegerValue)initialExpression.interpretResult();
        IntegerValue finalValue = (IntegerValue)finalExpression.interpretResult();

        id.assignValue(initialValue);
        while(((IntegerValue)id.interpretResult()).getValue()<=finalValue.getValue())
        {
            for(StatementNode stmnt:statements)
            {
                stmnt.Interpret();
            }

            int incValue = ((IntegerValue)id.interpretResult()).getValue()+1;
            id.assignValue(new IntegerValue(incValue) );
        }

    }

}
